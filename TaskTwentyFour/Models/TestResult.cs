﻿namespace TaskTwentyFour.Models
{
    public class TestResult : SumResult
    {
        public long ElapsedTime { get; set; }

        public TestResult(long sum, int threads, long time)
            : base(sum, threads)
        {
            ElapsedTime = time;
        }
    }
}
