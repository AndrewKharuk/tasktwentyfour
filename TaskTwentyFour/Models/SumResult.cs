﻿namespace TaskTwentyFour.Models
{
    public class SumResult
    {
        public long ArrayElementsSum { get; set; }

        public int ThreadsQuantity { get; set; }

        public SumResult(long sum, int threads)
        {
            ArrayElementsSum = sum;
            ThreadsQuantity = threads;
        }

    }
}
