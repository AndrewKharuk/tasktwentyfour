﻿using System.Linq;
using TaskTwentyFour.Models;

namespace TaskTwentyFour.Strategies
{
    public class PLINQStrategyTest : IStrategyTest
    {
        public SumResult GetResult(int[] array)
        {
            long sum = (long)array.AsParallel().Sum();

            return new SumResult(sum, 0);
        }
    }
}
