﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskTwentyFour.Models;

namespace TaskTwentyFour.Strategies
{
    public class TaskStrategyTest : IStrategyTest
    {
        readonly int threadsQuantity = Environment.ProcessorCount;
        List<Task> tasks = new List<Task>();
        int[] testArray;
        int count;

        public SumResult GetResult(int[] array)
        {
            testArray = array;
            count = array.Count();
            for (int i = 0; i < threadsQuantity; i++)
            {
                int i1 = i;
                tasks.Add(new Task<long>(() => GetThreadSum(i1)));
            }

            foreach (var task in tasks)
            {
                task.Start();
            }

            long allSum = 0;
            foreach (Task<long> task in tasks)
            {
                allSum += task.Result;
            }

            return new SumResult(allSum, threadsQuantity);
        }

        private long GetThreadSum(int i)
        {
            long sum = 0;

            var max = count * (i + 1) / threadsQuantity;
            for (int j = i * count / threadsQuantity; j < max; j++)
            {
                sum += testArray[j];
            }
            return sum;
        }
    }
}
