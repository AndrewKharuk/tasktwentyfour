﻿using TaskTwentyFour.Models;

namespace TaskTwentyFour.Strategies
{
    public interface IStrategyTest
    {
        SumResult GetResult(int[] array);
    }
}
