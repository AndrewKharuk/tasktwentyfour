﻿using TaskTwentyFour.Models;

namespace TaskTwentyFour.Strategies
{
    public class SingleStrategyTest : IStrategyTest
    {
        public SumResult GetResult(int[] array)
        {
            long sum = 0;

            foreach (var item in array)
            {
                sum += item;
            }

            return new SumResult(sum, 1);
        }
    }
}
