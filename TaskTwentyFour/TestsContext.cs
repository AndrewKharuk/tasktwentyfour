﻿using System.Diagnostics;
using TaskTwentyFour.Models;
using TaskTwentyFour.Strategies;

namespace TaskTwentyFour
{
    class TestsContext
    {
        private IStrategyTest Test { get; set; }

        public TestsContext() { }

        public TestsContext(IStrategyTest test)
        {
            Test = test;
        }

        public void SetTest(IStrategyTest test)
        {
            Test = test;
        }

        public TestResult ExecuteTest(int[] array)
        {
            var timer = Stopwatch.StartNew();
     
            SumResult sumResult = Test.GetResult(array);

            timer.Stop();


            return new TestResult(sumResult.ArrayElementsSum, sumResult.ThreadsQuantity, timer.ElapsedMilliseconds);
        }
    }
}
