﻿using System;
using TaskTwentyFour.Models;
using TaskTwentyFour.Strategies;

namespace TaskTwentyFour
{
    class Program
    {
        static int[] testArray;
        const int count = 100_000_000;
        static void Main(string[] args)
        {
            testArray = GetArray(count);

            TestsContext testsContext = new TestsContext(new SingleStrategyTest());

            WriteResult(testsContext.ExecuteTest(testArray));

            testsContext.SetTest(new TaskStrategyTest());
            WriteResult(testsContext.ExecuteTest(testArray));

            testsContext.SetTest(new PLINQStrategyTest());
            WriteResult(testsContext.ExecuteTest(testArray));

            Console.ReadKey();
        }

        static int[] GetArray(int count)
        {
            Random rnd = new Random();
            int[] array = new int[count];

            for (int i = 0; i < count; i++)
            {
                array[i] = rnd.Next(0, 10);
            }

            return array;
        }

        static void WriteResult(TestResult result)
        {
            string threads = result.ThreadsQuantity == 0 ? "-" : result.ThreadsQuantity.ToString();

            Console.WriteLine($"Sum: {result.ArrayElementsSum}, time: {result.ElapsedTime} ms, threads: {threads}.\n");
        }
    }
}
